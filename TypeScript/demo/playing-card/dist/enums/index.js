"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mark = exports.ColorEnum = void 0;
var ColorEnum;
(function (ColorEnum) {
    ColorEnum["heart"] = "\u2665";
    ColorEnum["spade"] = "\u2660";
    ColorEnum["diamond"] = "\u2666";
    ColorEnum["club"] = "\u2663";
})(ColorEnum || (exports.ColorEnum = ColorEnum = {}));
var Mark;
(function (Mark) {
    Mark["one"] = "A";
    Mark["two"] = "2";
    Mark["three"] = "3";
    Mark["four"] = "4";
    Mark["five"] = "5";
    Mark["six"] = "6";
    Mark["seven"] = "7";
    Mark["eight"] = "8";
    Mark["nine"] = "9";
    Mark["ten"] = "10";
    Mark["eleven"] = "J";
    Mark["twelve"] = "Q";
    Mark["thirteen"] = "K";
})(Mark || (exports.Mark = Mark = {}));

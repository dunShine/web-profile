"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.printDeck = exports.createDeck = void 0;
const enums_1 = require("../enums");
function createDeck() {
    const deck = [];
    let marks = Object.values(enums_1.Mark);
    let colors = Object.values(enums_1.ColorEnum);
    for (const m of marks) {
        for (const c of colors) {
            deck.push({
                color: c,
                mark: m,
                getString() {
                    return this.color + this.mark;
                }
            });
        }
    }
    let bigJoker = {
        type: 'big',
        getString() {
            return 'JO';
        }
    };
    let smallJoker = {
        type: 'small',
        getString() {
            return 'jo';
        }
    };
    return deck;
}
exports.createDeck = createDeck;
function printDeck(deck) {
    let result = '\n';
    deck.forEach((card, i) => {
        result += card.getString() + '\t';
        if ((i + 1) % 6 == 0) {
            result += '\n';
        }
    });
    console.log(result);
}
exports.printDeck = printDeck;

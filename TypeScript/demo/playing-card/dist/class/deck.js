"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Deck = void 0;
const enums_1 = require("../enums");
class Deck {
    constructor(cards) {
        this.cards = [];
        if (cards) {
            this.cards = cards;
        }
        else {
            this.init();
        }
    }
    init() {
        let marks = Object.values(enums_1.Mark);
        let colors = Object.values(enums_1.ColorEnum);
        for (const m of marks) {
            for (const c of colors) {
                this.cards.push({
                    color: c,
                    mark: m,
                    getString() {
                        return this.color + this.mark;
                    }
                });
            }
        }
        let bigJoker = {
            type: 'big',
            getString() {
                return 'JO';
            }
        };
        let smallJoker = {
            type: 'small',
            getString() {
                return 'jo';
            }
        };
        this.cards.push(bigJoker, smallJoker);
    }
    print() {
        let result = '\n';
        this.cards.forEach((card, i) => {
            result += card.getString() + '\t';
            if ((i + 1) % 6 == 0) {
                result += '\n';
            }
        });
        console.log(result);
    }
    shuffle() {
        for (let i = 0; i < this.cards.length; i++) {
            let targetIndex = this.getRandom(i, this.cards.length);
            let temp = this.cards[i];
            this.cards[i] = this.cards[targetIndex];
            this.cards[targetIndex] = temp;
        }
    }
    getRandom(min, max) {
        const dec = max - min;
        return Math.floor(Math.random() * dec + min);
    }
    publish() {
        let player1, player2, player3, left;
        player1 = this.takeCards(17);
        player2 = this.takeCards(17);
        player3 = this.takeCards(17);
        left = new Deck(this.cards);
        return {
            player1,
            player2,
            player3,
            left
        };
    }
    takeCards(n) {
        const cards = [];
        for (let i = 0; i < n; i++) {
            cards.push(this.cards.shift());
        }
        return new Deck(cards);
    }
}
exports.Deck = Deck;

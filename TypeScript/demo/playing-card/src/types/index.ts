import { ColorEnum, Mark } from '../enums';

export type Deck = Card[]

// export type NormalCard = {
//   color: ColorEnum
//   mark: Mark
// }


// 使用接口改造
export interface NormalCard extends Card{
  color: ColorEnum
  mark: Mark
}

export interface Joker extends Card{
  type: 'big' | 'small'
}

export interface Card {
  getString(): string
}

import { ColorEnum, Mark } from "../enums";
import { Card, Deck } from "../types";

/**
 * @description: 创建一副扑克牌
 * @return {*}
 */
export function createDeck():Deck{
  const deck: Deck = []
  let marks = Object.values(Mark);
  let colors = Object.values(ColorEnum);
  for (const m of marks) {    
    for (const c of colors) {
      deck.push({
        color: c,
        mark: m,
        getString(){
          return this.color + this.mark
        }
      } as Card)
    }
  }

  let bigJoker = {
    type: 'big',
    getString(){
      return 'JO'
    }
  }
  let smallJoker = {
    type: 'small',  
    getString(){
      return 'jo'
    }
  }
  // deck.push(bigJoker, smallJoker)
 
  return deck
}


/**
 * @description: 打印扑克牌
 * @return {*}
 */
export function printDeck(deck: Deck){
  let result = '\n'
  deck.forEach((card, i)=>{
    result += card.getString() + '\t'
    if((i+1)%6 == 0 ){
      result += '\n'
    }
  })
  console.log(result);
}
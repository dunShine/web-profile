import { Mark, ColorEnum } from "../enums";
import { Card } from "../types";

interface PublishResult{
  player1: Deck,
  player2: Deck,
  player3: Deck,
  left: Deck
}

export class Deck {
  private cards: Card[] = []

  constructor(cards?: Card[]) {
    if(cards){
      this.cards = cards
    }
    else{
      this.init()
    }
    
  }

  /**
   * @description: 初始化扑克牌
   * @return {*}
   */
  private init() {
    let marks = Object.values(Mark);
    let colors = Object.values(ColorEnum);
    for (const m of marks) {
      for (const c of colors) {
        this.cards.push({
          color: c,
          mark: m,
          getString() {
            return this.color + this.mark
          }
        } as Card)
      }
    }

    let bigJoker = {
      type: 'big',
      getString() {
        return 'JO'
      }
    }
    let smallJoker = {
      type: 'small',
      getString() {
        return 'jo'
      }
    }
    this.cards.push(bigJoker, smallJoker)
  }

  /**
   * @description: 打印扑克牌
   * @return {*}
   */  
  print() {
    let result = '\n'
    this.cards.forEach((card, i) => {
      result += card.getString() + '\t'
      if ((i + 1) % 6 == 0) {
        result += '\n'
      }
    })
    console.log(result);
  }

  /**
   * @description: 洗牌
   * @return {*}
   */  
  shuffle(){
    for(let i = 0; i < this.cards.length; i++){
      let targetIndex = this.getRandom(i, this.cards.length)
      let temp = this.cards[i]
      this.cards[i] = this.cards[targetIndex]
      this.cards[targetIndex] = temp
    }
  }

  /**
   * @description: 获取随机数, 无法取到最大值
   * @param {number} min
   * @param {number} max
   * @return {*}
   */  
  private getRandom(min: number, max:number){
    const dec = max - min
    return Math.floor(Math.random() * dec + min)
  }

  /**
   * @description: 发完牌后 得到4个card数组
   * @return {*}
   *       [Card[],Card[],Card[],[Card]]  元组
   */  
  publish(): PublishResult{
    //  const result: [Card[],Card[],Card[],Card[]] = [[],[],[],[]]
    let player1:Deck, player2:Deck, player3:Deck, left:Deck;
    player1 = this.takeCards(17);
    player2 = this.takeCards(17);
    player3 = this.takeCards(17);
    // left = this.takeCards(3);
    left = new Deck(this.cards);

    return {
      player1, 
      player2, 
      player3, 
      left
    };
  }

  /**
   * @description: 随机获取一副牌
   * @return {*}
   */  
  private takeCards(n: number): Deck{
    const cards: Card[] = [];
    for(let i = 0; i < n; i++){
      cards.push(this.cards.shift() as Card)
    }

    return new Deck(cards);
  }
}
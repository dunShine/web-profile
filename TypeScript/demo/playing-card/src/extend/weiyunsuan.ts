/**
 * @description: 枚举的位运算
 * @return {*}
 */

enum Permission {
  Read = 1,  // 0001
  Write = 2, // 0010
  Create = 4,// 0100
  Delete = 8 // 1000
}

// 1. 如何设置权限
// 使用位运算： 2个数字换算出2进制后的运算
let p = Permission.Read | Permission.Write;
console.log(p);


// 2. 如何判断是否拥有某个权限
function hasPermission(target: Permission, per: Permission){
  return (target & per) === per
}

// 判断变量P是否拥有可读权限
hasPermission(p, Permission.Read)

// 3. 如何删除某个权限
// 删除可读权限
// 0011
// 异或 ^
// 0001
// 0010
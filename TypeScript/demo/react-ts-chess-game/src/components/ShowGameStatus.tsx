import { ChessType, GameStatus } from "../types/enums"
import '../assets/style/ShowGameStatusComp.css'

interface IProps { 
  nextChess: ChessType.black | ChessType.red
  gameStatus: GameStatus
}
export const ShowGameStatus = (props: IProps) => {
  let content: JSX.Element;
  if (props.gameStatus === GameStatus.gaming) {
    if (props.nextChess === ChessType.black) {
      content = <div className="black">黑子落棋</div>
    }
    else {
      content = <div className="red">红子落棋</div>
    }
  }
  else if (props.gameStatus === GameStatus.redWin) {
    content = <div className="win red">红方胜利</div>

  }
  else if (props.gameStatus === GameStatus.blackWin) {
    content = <div className="win black">黑方胜利</div>

  }
  else { 
    content = <div className="win deuce">平局</div>

  }

  return (
    <div className="status">
      {content}
    </div>
  )
 }

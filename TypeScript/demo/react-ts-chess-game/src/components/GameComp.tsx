import React from "react";
import { BoardComp } from "./BoardComp";
import { ChessType, GameStatus } from "../types/enums";
import "../assets/style/GameComp.css";
import { ShowGameStatus } from "./ShowGameStatus";

interface IState {
  chesses: ChessType[];
  nextChess: ChessType.black | ChessType.red;
  gameStatus: GameStatus
}

export class GameComp extends React.Component<{}, IState> {
  state: IState = {
    chesses: [],
    nextChess: ChessType.black,
    gameStatus: GameStatus.gaming
  };

  componentDidMount(): void {
    this.init()
  }

  /**
   * @description: 初始化棋盘数据
   * @return {*}
   */
  init() { 
    const chesses: ChessType[] = []
    for (let i = 0; i < 9; i++) { 
      chesses.push(ChessType.null)
    }

    this.setState({
      chesses,
      nextChess: ChessType.black,
      gameStatus: GameStatus.gaming
    })
  }

/**
 * @description: 点击单个旗子触发的事件
 * @return {*}
 */
  handelChessClick(index: number) { 
    const chesses = [...this.state.chesses]
    chesses[index] = this.state.nextChess

    this.setState(prevState => ({ 
      chesses,
      nextChess: prevState.nextChess === ChessType.red ? ChessType.black : ChessType.red,
      gameStatus: this.getGameStatua(index, chesses)
    }))
  }

  /**
   * @description: 游戏状态判断核心逻辑
   * @param {number} index
   * @param {ChessType} chesses
   * @return {*}
   */  
  getGameStatua(index: number, chesses: ChessType[]): GameStatus {
    // 1. 其中一方胜利
    const herMin = Math.floor(index / 3) * 3
    const verMin = index % 3
    if ((chesses[herMin] === chesses[herMin + 1] && chesses[herMin] === chesses[herMin + 2])
      || (chesses[verMin] === chesses[verMin + 3] && chesses[verMin] === chesses[verMin + 6])
      || (chesses[0] === chesses[4] && chesses[0] === chesses[8] && chesses[0] !== ChessType.null)
      || (chesses[2] === chesses[4] && chesses[2] === chesses[6] && chesses[2] !== ChessType.null)
    ) { 
      if (chesses[index] === ChessType.red) {
        return GameStatus.redWin
      }
      else { 
        return GameStatus.blackWin
      }
    }

    // 2. 平局
    if (!chesses.includes(ChessType.null)) return GameStatus.deuce

    // 3. 游戏正在进行中
    return GameStatus.gaming

  }

  render(): React.ReactNode {
    return (
      <div className="game">
        <ShowGameStatus
          nextChess={this.state.nextChess}
          gameStatus={this.state.gameStatus}
        />
        <BoardComp
          chesses={this.state.chesses}
          onClick={this.handelChessClick.bind(this)}
          isGameOver={this.state.gameStatus !==  GameStatus.gaming}
        />
        <button onClick={() => { 
          this.init()
        }}>重新开始</button>
      </div>
    );
  }
}

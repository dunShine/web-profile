/**
 * 棋盘组件
 */
import React from "react"
import { ChessType } from "../types/enums"
import { ChessComp } from "./ChessComp"
import '../assets/style/BoardComp.css'

// import 
interface IProps { 
  chesses: ChessType[],
  onClick?: (index: number) => void
  isGameOver?: boolean
}
export const BoardComp: React.FC<IProps> = (props) => {
  // const isGameOver = props.isGameOver as Boolean
  const isGameOver = props.isGameOver!
  const list = props.chesses.map((type, i) =>  
    <ChessComp
      type={type}
      key={i}
      onClick={() => { 
        if (props.onClick && !isGameOver) { 
          props.onClick(i)
        }
      }}
    />
  )

  return (
    <div className="board">
      {list}
    </div>
  )
}

BoardComp.defaultProps = {
  isGameOver: false
}
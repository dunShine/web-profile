import { ChessType } from "../types/enums"
import '../assets/style/ChessComp.css'
/**
 * 旗子组件
 */
interface IProps { 
  type: ChessType
  onClick?: () => void 
}

export const ChessComp = (props: IProps) => { 
  let content: JSX.Element
  if (props.type === ChessType.red) {
    content = <div className="chess-item red" ></div>
  }
  else if (props.type === ChessType.black) {
    content = <div className="chess-item black" ></div>
  }
  else { 
    content = <div className="chess-item" ></div>
  }
  
  return (
    <div className="chess" onClick={() => { 
      if (props.type === ChessType.null && props.onClick) { 
        props.onClick()
      }
    }}
    >
      {content}
    </div>
  )
}